Создать образ

docker build -t react_with_nginx .

Запустить контейнер

docker run -d --rm --name myreact -p 8000:80 -v $(pwd)/default.conf:/etc/nginx/conf.d/default.conf react_with_nginx
