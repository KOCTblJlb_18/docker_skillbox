1) развернуть LDAP-сервер;

![ldap](screenshots/1_ldap_up.png)

2) добавить веб-интерфейс для созданного сервиса LDAP;

![phpldap](screenshots/2_phpldap_up.png)

![phpldapweb](screenshots/2.1_phpldap_web.png)


3) добавить GitLab;

![gitlab](screenshots/3_gitlab_up.png)

4) провести интеграцию GitLab с каталогом пользователей LDAP;

![gitlab_entr](screenshots/3.1_ldap_gitlab_entrance.png)

![gitlab_login](screenshots/3.2_gitbox_login_gitlab.png)

5) добавить пользователя в LDAP;

![ldap_user](screenshots/2.2_ldap_user.png)

6) добавить Rocket Chat в созданную структуру;

![mdb_rockch](screenshots/4_mongodb_rocketchat_up.png)

![ldap_rockch](screenshots/4.1_check_LDAP_in_rocketchat.PNG)

![rockch_entr](screenshots/4.2_rocketchat_entrance.png)

7) провести интеграцию Rocket Chat с каталогом пользователей LDAP.

![rockch_user_gitbox](screenshots/4.3_user_gitbox_in_rocketchat.PNG)