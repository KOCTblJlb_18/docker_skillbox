from flask import Flask
from redis import Redis
import socket

app = Flask(__name__)
redis = Redis(host='redis', port=6379)

@app.route('/')
def hello():
    count = redis.incr('hits')    
    return 'Hello! Here is skillbox! <p> Hostname is: ' + socket.gethostname() + '<p> Version 3 <p> Count {} times.\n'.format(count)

if __name__ == '__main__':
	app.run(host='0.0.0.0', port=8080, debug=True)
